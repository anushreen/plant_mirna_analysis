#!/usr/bin/env python
    
__author__           = "Anushree Narjala"
__copyright__        = "Copyright 2019, Anushree Narjala and NCBS Bangalore"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Anushree Narjala"
__email__            = "anushreen@ncbs.res.in"
__status__           = "Development"


"""Usage: python calculate_target_gc.py <metadata.tsv> <transcripts.fa>

Find the target positions on the mRNA and calculate GC content. 
Metadata is the output of psRNATarget

"""

import sys
import os
import pandas as pd
import matplotlib as mpl
#import matplotlib.axis as ax
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from collections import defaultdict
import difflib

try:
    mpl.style.use( 'seaborn-talk' )
except Exception as e:
    pass
plt.rc('font', family='serif')
plt.rcParams['axes.linewidth'] = 2

mirnaTarget_ = defaultdict( list )

def findSeqOfTarget( target, seqs ):
    return filter( lambda x: target in x.split(' ')[0], seqs )

def calcGC( seq ):
    ng = seq.count( 'G' )
    nc = seq.count( 'C' )
    return float( ng + nc ) / len( seq )

def plotGCProfile( siteN, index, GC, offset  ):
    plt.plot( index -  siteN, GC, color="grey", linewidth=0.6)
    X = index-siteN
    plt.ylim( [ 0.0, 1.0 ] )
    plt.xlim( [ -offset, offset] )
    plt.axvline(x=0, ymin=0, ymax = 1, linewidth=0.2, color="red")
    

def computeGC( miRNASite, target_site, seq, offset):
    print(target_site)
    assert target_site in seq 
    N = len( seq )
    #W = len( target_site )
    W = 21
    
    gc = np.zeros( N - W )
    gc1 = gc.copy()
    x = np.arange( 0, N - W, 1 )
    
    miRNAgc = calcGC(miRNASite)

    # Here my target site starts
    siteN = seq.index( target_site )
    for i in x:
        gc[i] = calcGC( seq[i:i+W] )
        gc1[i] = calcGC( seq[i:i+3*W] )
    
    a, b = max(0,siteN-offset), min(siteN+offset+1,len(x))
    xx = x[a:b]
    yy = gc[a:b]
    return siteN, xx, yy

def process( meta, fasta ):
    global mirnaTarget_ 
    print( 'Total lines in meta data : %d' % len( meta ) )
    print( 'Total lines in fasta : %d' % len( fasta ) )
    
    siteN = None
    offset = 200
    df = pd.DataFrame( )
    GCs = []
    indices = []
    siteNs = [ ]
    targets = [ ]
    
    for line in meta:
        if len( line.strip() ) < 1:
            continue
        line = line.split( '\t' )
        mirnaTarget_[ line[1] ].append( line )

    for i, tgt in enumerate( mirnaTarget_ ):
        matches = findSeqOfTarget( tgt, fasta )
        print( '[INFO] For target %s found total %d matches' % (tgt,
            len(matches) )
            )
        for mirnaLine in mirnaTarget_[ tgt ]:
            targetSite = mirnaLine[9].replace( 'U', 'T' ).replace('-', '')
            miRNASite = mirnaLine[8].replace( 'U', 'T' ).replace('-', '')
            for match in matches:
                seq = match.split( '\n' )[1].upper()
                siteN, index, GC = computeGC(miRNASite, targetSite, seq, offset)
                indices.append(index-siteN)
                GCs.append(GC)
                siteNs.append( siteN )
                targets.append( tgt )

    df[ 'indices' ] = indices 
    df[ 'gcs' ] = GCs
    df[ 'target location' ] = siteNs
    df[ 'mirna target' ] = targets
    return df

def plotAndSave( df, outfile = None ):
    ax = plt.gca()
    ax.tick_params(direction='out')
    nrows = df[ 'mirna target' ].size
    print(nrows)
    print df

    minI, maxI = 0, 0
    for i, row in df.iterrows( ):
        indices = row[ 'indices' ]
        minI = min( minI, min( indices ) )
        maxI = max( maxI, max( indices) )
    data = np.zeros( shape=(nrows,maxI-minI+1) )
    for i, row in df.iterrows( ):
        indices = row[ 'indices' ]
        gcs = row[ 'gcs' ]
        for ri, g in zip( indices, gcs ):
            data[i,ri-minI] = g

    # We must ignore zeros. replace 0 with NaN and use numpy special functions
    # to calculate mean and std which ignores NaN.
    data[ data == 0 ] = np.nan
    xvec = np.arange(0, data.shape[1]) - int(data.shape[1]/2)
    
    mean = np.nanmean( data, axis = 0 )
    err = np.nanstd( data, axis = 0 )
    plt.plot( xvec, mean, color = 'blue', lw = 2 )
    plt.fill_between( xvec, mean + err, mean - err
            , alpha = 0.3, color='blue' 
            )
    plt.xlim( [ minI, maxI ] )
    plt.ylim( [0.2, 0.8] )
    plt.axvline(x=0, ymin=0, ymax = 1, linewidth=0.1, color="grey")
    plt.axhline(xmin=minI, xmax=maxI, y= 0.5, linewidth=0.1, color="grey")
    ax.tick_params(top="off", right="off")

    if outfile is None:
        plt.show( )

    else:
        plt.savefig( outfile, dpi=300 )
        print( 'Saved to %s' % outfile )

def main( ):
    with open( sys.argv[1], 'r' ) as f:
        metaData = filter( None, f.read().split( '\n' ) )
    with open( sys.argv[2], 'r' ) as f:
        fastaData = filter( None, f.read( ).split( '\n>' ) )
    df = process( metaData, fastaData )

    outfile = '%s.png' % sys.argv[1] 
    plotAndSave( df, outfile )

if __name__ == '__main__':
    main()
