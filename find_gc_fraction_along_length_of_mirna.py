#!/usr/bin/env python
    
__author__           = "Anushree Narjala"
__copyright__        = "Copyright 2019, Anushree Narjala and NCBS Bangalore"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Anushree Narjala"
__email__            = "anushreen@ncbs.res.in"
__status__           = "Development"

"""find_gc_fraction_along_length_of_mirna.py fasta/txt 

"""
import pylab
import numpy as np
import sys
import os
from collections import defaultdict

infile_ = None

# All microRNAs are stored in this dictionary. Key is the length of the
# microRNAs. All of them may not be of length 21.
micro_rna_ = defaultdict( list )

def get_column( rnas, column_index ):
    cols = []
    for rna in rnas:
        cols.append( rna[ column_index ] )
    return ''.join(cols)

def get_gc_content( seq ):
    return seq.count('G'), seq.count( 'C' )

def get_augc_count( seq ):
    return [ seq.count(x) for x in [ 'A', 'U', 'G', 'C' ] ]

def gc_in_colums( rnas ):
    yvec = []
    if not rnas:
        return []
    for i in range( len( rnas[0] ) ):
        col = get_column( rnas, i )
        count = get_augc_count( col )
        yvec.append ( float(count[2] + count[3]) / sum( count ) )
    return yvec

def process_mirna_with_same_length( rnas, length ):
    global infile_
    data = []
    numSeqs = []
    allgc = gc_in_colums(rnas)
    numSeqs.append(len(rnas))
    data.append(allgc)
    for i in range( length ):
        rnaWithAorUAtPos = filter( lambda x: x[i] in [ 'A', 'U' ], rnas )
        gcList = gc_in_colums( rnaWithAorUAtPos )
        numSeqs.append( len( rnaWithAorUAtPos ) )
        if not gcList:
            print( '[INFO] Position %d does not have any U or A' % i )
            continue
        data.append( gcList )
        print( '==================' )
    data = np.array( data )
    pylab.figure( )
    pylab.imshow( data, interpolation = 'none', cmap = 'seismic', vmin=0,vmax=1 )
    pylab.colorbar( ).ax.tick_params(axis='y', direction='out')
    #pylab.title( 'GC Signature in miRNAs of length %s' % length )
    outfile = '%s_length%s_gc_signature.png' % ( infile_, length )
    csvfile = '%s_length%s_gc_signalure.csv' % ( infile_, length )
    pylab.xlabel( 'Position of nucleotide along the length of miRNA' )
    pylab.ylabel( '(Position in seq where A/U is present, #Seq)' )
    height, width = data.shape
    pylab.xticks( range(width), range(1, width + 1 ) )
    pylab.yticks( range(height), zip( range(0, height), numSeqs ) )
    pylab.tick_params(axis='y', direction='out')
    pylab.tick_params(axis='x', direction='out')
    pylab.tick_params(top=False)
    pylab.tick_params(right=False)
    pylab.savefig( outfile, dpi=300 )
    np.savetxt( csvfile, data )
    #print data
    print( '[INFO] Wrote to %s' % outfile  )
    print( '[INFO] Wrote data to %s' % csvfile )
    

def process( filename ):
    with open( filename, 'r' ) as f:
        lines  = filter(None, f.read( ).split( '\n' ))
    print('[INFO] Total lines in file %d' % len( lines ) )
    for mirna in lines:
        mirna = mirna.replace( 'T', 'U' )
        micro_rna_[ len(mirna) ].append( mirna )

    for mirnaLen in micro_rna_:
        if(mirnaLen >=20 and mirnaLen <=24):        
            process_mirna_with_same_length( micro_rna_[mirnaLen], mirnaLen )

def main( ):
    global infile_
    infile_ = sys.argv[1]
    print( '[INFO] Processing file %s' % infile_ )
    process( infile_ )


if __name__ == '__main__':
    main()
