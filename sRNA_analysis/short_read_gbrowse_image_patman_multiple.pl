# Perl script for generating 'gbrowse'-like images in png format from fasta files
# Copyright (C) 2009 Thomas J. Hardcastle <tjh48@cam.ac.uk> & Frank Schwach <f.schwach@uea.ac.uk>

# Modified by Anushree <anushreen@ncbs.res.in> Copyright (C) 2019 -
#

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# Description
# Generates a genome browser like image showing the
# positions of short sequences on a reference sequence limited by coordinates given
# 
# Input: 
#   - BED file of smallRNA sequences with 5th column as normalized abundance - 3 bed files
#   - FASTA file of reference sequence (only first sequence will be used)
#   - name of a directory for the intermediate and result files
#     (will be created if it doesn't exist)
# Filters for length and abundance 


use strict ;
use warnings;
use Bio::SeqIO;
use Bio::Graphics;
use Bio::SeqFeature::Generic;
use Bio::Tools::GFF;
use Getopt::Long;
use GD;
use GD::Image;
use SVG;

#declare variables
my $usage = "\nUsage: $0 -P1 SRNA_FILE_BED -P2 SRNA_FILE_BED -P3 SRNA_FILE_BED -D REF_SEQ_FASTA_FILE -B BED_FILE -min MIN_SIZE -max MAX_SIZE -o OUTPUT_DIR -n RESULT_NAME -low LOWER_LIMIT -upp UPPER_LIMIT -h PNG_HEIGHT -w PNG_WIDTH -ta THICK_ARROWS(0/1) -c COLORS(0/1) -gffo GFF_FILE -gfft GFF_FILE \n";
##perl short_read_gbrowse_image_patman_multiple.pl -P1 VC.bed -P2 amiR_rep1.bed -P3 amiR_rep2.bed -D AmiR.fa -B AmiR.bed -min 20 -max 24 -o short_read_png -n $name -ta 1 -c 1 -gffo $stemloop.gff -gfft $miRNA.gff
my $srna_file1 ;
my $srna_file2 ;
my $srna_file3 ;
my $refseq_file ;
my $bedfile;
my $result_dir ;
my $result_name ;
my $min_srna_length = 20 ;
my $max_srna_length = 25 ;
my $lowlim;
my $upplim;
my $ushgt = 1100;
my $uswid = 800;
my $thickarr = 0;
my $usecol = 0;
my $gfffile1;
my $gfffile2;
my $ref_strand;
my %hits;

# Get ARGS
GetOptions(
  'P1=s'    => \$srna_file1,
  'P2=s'    => \$srna_file2,
  'P3=s'    => \$srna_file3,
  'D=s'    => \$refseq_file,
  'B=s'    => \$bedfile,
  'min=i'  => \$min_srna_length,
  'max=i'  => \$max_srna_length,
  'o=s'    => \$result_dir,
  'n=s'    => \$result_name,
  'low=i'  => \$lowlim,
  'upp=i'  => \$upplim,
  'h=i' => \$ushgt,
  'w=i' => \$uswid,
  'ta=i' => \$thickarr,
  'c=i' => \$usecol,
  'gffo=s' => \$gfffile1,
  'gfft=s' => \$gfffile2,
) ;

# test ARGS
die $usage ."\n No short RNA sequence file given in bed format\n" unless $srna_file1;
die $usage ."\n No reference sequence file given\n" unless $refseq_file;
die $usage ."\n No bed file given\n" unless $bedfile;
die $usage ."\n No output directory name given\n" unless $result_dir;
die $usage ."\n No output name given\n" unless $result_name;

die "Could not read input short RNA sequence file\n" unless -r $srna_file1;
die "Could not read reference sequence file\n" unless -r $refseq_file ;
die "Could not read reference bed file\n" unless -r $bedfile;
  
# create output directory if it doesn't exist
mkdir $result_dir unless -d $result_dir;

# Read files

# Reference sequence:
# write to new file to ensure that there 
# is only one sequence to map to
my $parser = Bio::SeqIO->new(-format => 'fasta' , -file => $refseq_file) ;
while (my $seq_obj = $parser -> next_seq) 
{
    my $refseq_length = $seq_obj -> length ;
    my $refseq = $seq_obj -> seq;
    my $refseq_id = $seq_obj -> id;
    my $refseq_file_patman = $result_dir.'/refseq_patman.fasta' ;
    open (REFSEQ_PAT , '>', $refseq_file_patman) || die "Could not create temp file for patman (refseq)\n" ;
    print REFSEQ_PAT '>'.$seq_obj->id."\n".$seq_obj->seq ."\n";
    close REFSEQ_PAT ;
    
    open(BED_IN, $bedfile) or die " could not open bed file\n" ;
    while ( my $bedline = <BED_IN>) 
    {
        my ($bchrom, $bstart, $bend, $bname, $bscore, $bstrand ) = split /\t/, $bedline;
		#change this line for multiple bed lines
        #if ($bname eq $refseq_id)
        if ($bchrom eq $refseq_id)
        {
            $lowlim = $bstart;
            $upplim = $bend;
            $bstrand =~ s/^\s+//;
            $bstrand =~ s/\s+$//;
            $ref_strand = $bstrand;
            
        }
    }    
    # Initialize genome browser view
    # with one track for the sRNA arrows
    # and one for the ruler
    my $panel = Bio::Graphics::Panel->new(-start => $lowlim,
				                          -stop => $upplim,
                                          -width  => $uswid,
				                          -height => $ushgt,
                                          -pad_left => 15,
                                          -pad_right => 25,
                                         # -image_class => 'GD::SVG',
                                          );
    # Add ruler
    my $full_length = Bio::SeqFeature::Generic->new(-start=>$lowlim,
                                                    -end=>$upplim, 
                                                    -strand => $ref_strand,
                                                    -display_name => $refseq_id,
                                                    -seq => $refseq,
                                                    );
    $panel->add_track($full_length,
                      -glyph   => 'arrow',
                      -tick    => 2,
                      -fgcolor => 'black',
                      -label      => 1,
                      -linewidth  => 2,
                      -description => 1,
                      #-do_gc => 1,
                      -font    => gdGiantFont,
                    );
    
	my $gff1 = Bio::Tools::GFF->new(-file => $gfffile1, -gff_version => 3);
	my $feature1;
	my $tag1;
	my %hash1;
	while($feature1 = $gff1->next_feature()) {
	  $tag1 = $feature1->primary_tag;
	  push @{$hash1{$tag1}}, $feature1;
	}
	$gff1->close();

	my @colors1 = qw(firebrick black );
	my $idx1    = 0;
	for my $tag1 (sort keys %hash1) {
	  my $features = $hash1{$tag1};
	  $panel->add_track($features,
		            -glyph       =>  'arrow',
		            -fgcolor     => $colors1[$idx1++ % @colors1],
		            -font2color  => 'black',
		            -font    => gdGiantFont,
		            -linewidth  => 4,
		            -bump        => +1,
		            -height      => 8,
		            -description => 1,
		           );
	   }

	#add GFF track
	my $gff = Bio::Tools::GFF->new(-file => $gfffile2, -gff_version => 3);
	my $feature;
	my $tag;
	my %hash;
	while($feature = $gff->next_feature()) {
	  $tag = $feature->primary_tag;
	  push @{$hash{$tag}}, $feature;
	}
	$gff->close();

	my @colors = qw(firebrick black);
	my $idx    = 0;
	   
    # Add track for sRNAs
    my $srna_track1 = $panel->add_track( -glyph     => 'arrow',
                                        -label      => 0,
                                        -fgcolor    => \&fgcolor,
                                        -bgcolor    => \&fgcolor,
                                        -linewidth  => \&line_width_from_score,
                                        -height     => \&line_height_from_score,
                                        -pad_bottom => 1,
                                        #-display_name => 'VC',
                                        -description => 1,
                                        );
    $panel->add_track($full_length,
                      -glyph   => 'line',
                      -fgcolor => 'black',
                      -linewidth  => 1,
                      -height => 2
                    );
    my $srna_track2 = $panel->add_track( -glyph      => 'arrow',
                                        -label      => 0,
                                        -fgcolor    => \&fgcolor,
                                        -bgcolor    => \&fgcolor,
                                        -linewidth  => \&line_width_from_score,
                                        -height     => \&line_height_from_score,
                                        #-display_name => 'AmiR_rep1',
                                        );
    $panel->add_track($full_length,
                      -glyph   => 'line',
                      -fgcolor => 'black',
                      -linewidth  => 1,
                      -height => 2
                    );
    my $srna_track3 = $panel->add_track( -glyph      => 'arrow',
                                        -label      => 0,
                                        -fgcolor    => \&fgcolor,
                                        -bgcolor    => \&fgcolor,
                                        -linewidth  => \&line_width_from_score,
                                        -height     => \&line_height_from_score,
                                        #-display_name => 'AmiR_rep2',
                                        );
    $panel->add_track($full_length,
                      -glyph   => 'line',
                      -fgcolor => 'black',
                      -linewidth  => 1,
                      -height => 2
                    );
    
    # parse bed file and add arrow to
    # genome browser view for each match
    open(PAT_IN1, $srna_file1) or die " could not open sRNA alignment file - 1\n" ;
    while ( my $patline = <PAT_IN1>) 
    {
      # Bed format : chrom, start, stop, sequence, RPM, strand)
      my ($chrom, $start, $stop, $seq, $seq_count, $strand) = split /\t/, $patline;
      $strand =~ s/^\s+//;
      $strand =~ s/\s+$//;
      if ($chrom eq $refseq_id ) 
      {
        if ($start >= $lowlim && $stop <= $upplim) 
        {
	        my $size = length $seq ;
	        if($size >= $min_srna_length && $size <= $max_srna_length && $seq_count >= 1)
	        { 
			    $hits{wt}{$refseq_id}{$seq}{count} = $seq_count;
			    $hits{wt}{$refseq_id}{$seq}{strand} = $strand;
			    # choose redundant sequences as thick arrows or not...
			    if ($thickarr == 1) 
			    {
			         my $feature = Bio::SeqFeature::Generic->new(-display_name=> $seq,
									    -start       => $start,
									    -end         => $stop,
									    -strand      => $strand,
									    -score       => $seq_count,);
			        $srna_track1->add_feature($feature);
			    } 
			}
        }
      }
      else { next ;}         
    } # parser (bed file)
    close PAT_IN1;
    
    open(PAT_IN2, $srna_file2) or die " could not open sRNA alignment file - 2\n" ;
    while ( my $patline = <PAT_IN2>) 
    {
      # Bed format : chrom, start, stop, sequence, RPM, strand)
      my ($chrom, $start, $stop, $seq, $seq_count, $strand) = split /\t/, $patline;
      $strand =~ s/^\s+//;
      $strand =~ s/\s+$//;
      if ($chrom eq $refseq_id ) 
      {
        if ($start >= $lowlim && $stop <= $upplim) 
        {
	        my $size = length $seq ;
	        if($size >= $min_srna_length && $size <= $max_srna_length && $seq_count >= 1)
	        { 
			    $hits{oe}{$refseq_id}{$seq}{count} = $seq_count;
			    $hits{oe}{$refseq_id}{$seq}{strand} = $strand;
			    
			    # choose redundant sequences as thick arrows or not...
			    if ($thickarr == 1) 
			    {
			         my $feature = Bio::SeqFeature::Generic->new(-display_name=> $seq,
									    -start       => $start,
									    -end         => $stop,
									    -strand      => $strand,
									    -score       => $seq_count,);
			        $srna_track2->add_feature($feature);
			    } 
			}
        }
      }
      else { next ;}         
    } # parser (bed file)
    close PAT_IN2;
    
    open(PAT_IN3, $srna_file3) or die " could not open sRNA alignment file - 3\n" ;
    while ( my $patline = <PAT_IN3>) 
    {
      # Bed format : chrom, start, stop, sequence, RPM, strand)
      my ($chrom, $start, $stop, $seq, $seq_count, $strand) = split /\t/, $patline;
      $strand =~ s/^\s+//;
      $strand =~ s/\s+$//;
      if ($chrom eq $refseq_id ) 
      {
        if ($start >= $lowlim && $stop <= $upplim) 
        {
	        my $size = length $seq ;
	        if($size >= $min_srna_length && $size <= $max_srna_length && $seq_count >= 1)
	        { 
			    $hits{kd}{$refseq_id}{$seq}{count} = $seq_count;
			    $hits{kd}{$refseq_id}{$seq}{strand} = $strand;
			    # choose redundant sequences as thick arrows or not...
			    if ($thickarr == 1) 
			    {
			        my $feature = Bio::SeqFeature::Generic->new(-display_name=> $seq,
									    -start       => $start,
									    -end         => $stop,
									    -strand      => $strand,
									    -score       => $seq_count,);
			        $srna_track3->add_feature($feature);
			    }
			 }
        }
      }
      else { next ;}         
    } # parser (bed file)
    close PAT_IN3;
    # generate image
    $refseq_id =~ s/\//_/;
    my $image_file = $result_dir.'/'.$refseq_id.'.png' ;
    open (IMAGE_FILE, '>', $image_file) || die "Could not generate image file\n" ;
    print IMAGE_FILE $panel->png;
    close IMAGE_FILE ;
}

my $stats_file = $result_dir.'/'.$result_name.'_stats.txt' ;
open (STATS_FILE, '>', $stats_file) || die "Could not generate stats file\n" ;
print STATS_FILE "Sample\tname\tsc21\tplus21\tminus21\tsc22\tplus22\tminus22\tsc24\tplus24\tminus24\tA21\tT21\tC21\tG21\tA22\tT22\tC22\tG22\tA24\tT24\tC24\tG24\n";
foreach my $sample (sort keys %{hits}) {
    foreach my $name (sort keys %{$hits{$sample}} ) {
        my $sc21 = 0; my $sc22 = 0; my $sc24 =0; my $plus21 = 0; my $minus21 = 0; my $plus22 = 0; my $minus22 = 0; my $plus24 = 0; my $minus24 = 0;
        my $A21 = 0; my $T21 = 0; my $C21 = 0; my $G21 = 0;  my $A22 = 0; my $T22 = 0; my $C22 = 0; my $G22 = 0; my $A24 = 0; my $T24 = 0; my $C24 = 0; my $G24 = 0;
        foreach my $seq (keys %{ $hits{$sample}{$name} }){
            foreach my $count (keys %{ $hits{$sample}{$name}{$seq} }){
                if(length $seq == 21){
                    $sc21 += $hits{$sample}{$name}{$seq}{count} ; 
                    if($hits{$sample}{$name}{$seq}{strand} eq '+') {
                        $plus21 += $hits{$sample}{$name}{$seq}{count}; }
                    elsif($hits{$sample}{$name}{$seq}{strand} eq '-') {
                        $minus21 += $hits{$sample}{$name}{$seq}{count};}
                    if($seq =~ /^A/) { $A21 += $hits{$sample}{$name}{$seq}{count};}
                    elsif($seq =~ /^T/ or $seq =~ /^U/) { $T21 += $hits{$sample}{$name}{$seq}{count};}
                    elsif($seq =~ /^C/) { $C21 += $hits{$sample}{$name}{$seq}{count};}
                    elsif($seq =~ /^G/) { $G21 += $hits{$sample}{$name}{$seq}{count};}
                } elsif(length $seq == 22){
                    $sc22 += $hits{$sample}{$name}{$seq}{count} ; 
                    if($hits{$sample}{$name}{$seq}{strand} eq '+') {
                        $plus22 += $hits{$sample}{$name}{$seq}{count}; }
                    elsif($hits{$sample}{$name}{$seq}{strand} eq '-') {
                        $minus22 += $hits{$sample}{$name}{$seq}{count};}
                    if($seq =~ /^A/) { $A22 += $hits{$sample}{$name}{$seq}{count};}
                    elsif($seq =~ /^T/ or $seq =~ /^U/) { $T22 += $hits{$sample}{$name}{$seq}{count};}
                    elsif($seq =~ /^C/) { $C22 += $hits{$sample}{$name}{$seq}{count};}
                    elsif($seq =~ /^G/) { $G22 += $hits{$sample}{$name}{$seq}{count};}
                }elsif(length $seq == 24){
                    $sc24 += $hits{$sample}{$name}{$seq}{count} ; 
                    if($hits{$sample}{$name}{$seq}{strand} eq '+') {
                        $plus24 += $hits{$sample}{$name}{$seq}{count}; }
                    elsif($hits{$sample}{$name}{$seq}{strand} eq '-') {
                        $minus24 += $hits{$sample}{$name}{$seq}{count};}
                    if($seq =~ /^A/) { $A24 += $hits{$sample}{$name}{$seq}{count};}
                    elsif($seq =~ /^T/ or $seq =~ /^U/) { $T24 += $hits{$sample}{$name}{$seq}{count};}
                    elsif($seq =~ /^C/) { $C24 += $hits{$sample}{$name}{$seq}{count};}
                    elsif($seq =~ /^G/) { $G24 += $hits{$sample}{$name}{$seq}{count};}
                }
             }
        }
        print STATS_FILE "$sample\t$name\t$sc21\t$plus21\t$minus21\t$sc22\t$plus22\t$minus22\t$sc24\t$plus24\t$minus24\t$A21\t$T21\t$C21\t$G21\t$A22\t$T22\t$C22\t$G22\t$A24\t$T24\t$C24\t$G24\n";
    }
}

exit 0;

################## subroutines
sub fgcolor {
    return "black" if $usecol == 0;
   
  my $feature=shift;
  my $len = ($feature->end - $feature->start) ; 
  return "pink" if $len >=15 and $len <=20; 
  return "red" if $len == 21; 
  return "green" if $len == 22;
  #return "yellow" if $len ==23; 
  return "blue" if $len == 24 ; 
  return "gray" ;
}
  
sub line_width_from_score {
  my $feature=shift; 
  my $score = $feature->score; 
  if ($score){
    return ((log($score)/log(10))*2)+2 
  } else{
    return 1
  } 
}
sub line_height_from_score {
  my $feature=shift; 
  my $score = $feature->score; 
  if ($score){
    return ((log($score)/log(10))*5)+5 
  }else{
    return 5
  } 
}
