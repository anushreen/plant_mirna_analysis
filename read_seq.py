#!/usr/bin/env python
    
__author__           = "Anushree Narjala"
__copyright__        = "Copyright 2019, Anushree Narjala and NCBS Bangalore"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Anushree Narjala"
__email__            = "anushreen@ncbs.res.in"
__status__           = "Development"


from collections import defaultdict 

def read_fasta( filename, unique = False ):
    """Read sequences from a fasta file.
    Return a defaultdict 
    """
    print("[INFO] Reading %s, Are collecting only unique: %s" % (filename, unique))
    if unique == True:
        seqsDict = defaultdict( set )
    else:
        seqsDict = defaultdict(list)
    with open( filename, "r") as f:
        text = f.read()
    blocks = text.split('>')
    for b in blocks:
        data = filter(None, b.split('\n'))
        if data:
            #header, seq = data[0], data[1:]
            header, seq = data[1].strip().replace('U', 'T'), data[1].strip().replace('U', 'T')
            if unique == True:
                #seqsDict[header].add("".join(seq))
                seqsDict[header].add(seq)
            else:
                seqsDict[header].append("".join(seq))
    return seqsDict

def read_text( filename, unique = True ):
    print("[INFO] Reading %s, Are collecting only unique: %s" % (filename, unique))
    if unique == True:
        seqsDict = defaultdict( set )
    else:
        seqsDict = defaultdict(list)
    with open( filename, "r") as f:
        text = f.read()
    lines = text.split('\n')
    for l in lines:
        l = l.split()
        #strip()
        if not l:
            continue
        header, seq = l[0].strip().replace('U', 'T'), l[0].strip().replace('U', 'T')
        if unique == True:
            seqsDict[header].add(seq)
        else:
            seqsDict[header].append(seq)
    return seqsDict
