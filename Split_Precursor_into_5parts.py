#!/usr/bin/env python
    
__author__           = "Anushree Narjala"
__copyright__        = "Copyright 2019, Anushree Narjala and NCBS Bangalore"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Anushree Narjala"
__email__            = "anushreen@ncbs.res.in"
__status__           = "Development"

'''Usage: python Split_Precursor_into_5parts.py <precursors.fa> <matures.fa> <outfile.csv>

Both the input files must have same base headers, except in mature.fa the headers are 
suffixed with -5p or -3p 
'''

import pylab
import numpy as np
import scipy
import sys
import os
from Bio import SeqIO, SeqUtils
from collections import defaultdict
import matplotlib.pyplot as plt

subseqs_ = None
outfile_ = None
precursor_ = None
splitDict = None
subSeqDict = defaultdict(list)

split1GCList = []
split2GCList = []
split3GCList = []
p3GCList = []
p5GCList = []
precursorGCList = []

resultDict = {}

def split(seqA, listSubseq):
    assert type(listSubseq) == list, "Expected list, got %s" % type(listSubseq)
    assert type(seqA) == list
    precursorSeq = seqA[0][0][0]
    p3Seq = None
    p5Seq = None
    for s in listSubseq:
        sid = s.id
        if '-3p' in sid: 
            p3Seq = s.seq
            p3GCList.append(SeqUtils.GC(s.seq))
        elif '-5p' in sid:
            p5Seq = s.seq
            p5GCList.append(SeqUtils.GC(s.seq))
        else: pass

        haystacks = seqA[-1]
        for haystack in haystacks:
            splitOn = str('%s'%s.seq)
            seqA.append([x.split(splitOn) for x in haystack])

    # This flatten the lists of list
    result = reduce(lambda x, y: x + y, seqA[-1])
    if len(result) == 2: pass
    else:
        split1, split2, split3 = result
        split1GCList.append(SeqUtils.GC(split1))
        split2GCList.append(SeqUtils.GC(split2))
        split3GCList.append(SeqUtils.GC(split3))
        resultDict[precursorSeq] = [split1, str(p5Seq), split2, str(p3Seq), split3]
    return result

def plotGCBar(filename):
    fig, ax = pylab.subplots()
    dataList = [split1GCList, p3GCList, split2GCList, p5GCList, split3GCList]
    mean = [np.mean(l) for l in dataList]
    std = [np.std(l) for l in dataList]
    ind = np.arange(len(mean))
    width = 0.35
    ax.bar(ind, mean, width, color='gray', yerr=std, ecolor='k')
    ax.set_xticks(ind+width)
    pylab.ylim(ymax = 80, ymin = 0)
    pylab.title("GC content across precursors (%s)" % filename)
    ax.set_xticklabels(('Before', '5P', 'Loop', '3P', 'After'))
        
    if filename:
        print("[INFO] Writing image to %s" % filename)
        pylab.savefig(filename)
    else:
        pylab.show()
   
def plotGCBox(filename):
    fig, ax = pylab.subplots()
    dataList = [split1GCList, p3GCList, split2GCList, p5GCList, split3GCList]
    plt.boxplot(dataList)
    plt.ylabel("GC percentage")
    plt.xticks([1,2,3,4,5], ["before", "3p", "loop", "5p", "after"])

    if filename:
        print("[INFO] Writing image to %s" % filename)
        pylab.savefig(filename)
    else:
        pylab.show()

 
def plotGCLine(filename):
    '''Plot line graph for GC'''
    pylab.figure()
    maxPrecursorLength = max([len(pre) for pre in resultDict.keys()])
    xvec = np.arange(maxPrecursorLength)

    windowSize = 100
    for pre in resultDict:
        vals = resultDict[pre]
        for i, v in enumerate(vals):
            pointY = [SeqUtils.GC(v), SeqUtils.GC(v)]
            pointX = [windowSize*i, windowSize*i+ len(v)]
            pylab.plot(pointX, pointY, 'o-')
            pylab.ylim(ymax = 100, ymin = 0)
    if filename:
        pylab.savefig(filename)
    else:
        pylab.show()


def splitPrecursor(subseqDict, outfile):
    '''Given a precursor and subseq, split precursor at the places where subseq
    are found
    '''
    with open(outfile, 'w') as csvF:
        header = [ "precursor_id"
                , "precursor"
                , "GC"
                , "subseq1_id"
                , "subseq1"
                , "GC"
                , "subseq2_id"
                , "subseq2"
                , "GC"
                , "splits (GC)"
                ]
        csvF.write(",".join(header))
        csvF.write("\n")

        for precursor in subseqDict:
            line = []
            subseqs = subseqDict[precursor]
            precursorSeq = str(precursor_[precursor].seq)
            line.append(precursor)
            line.append(precursorSeq)
            line.append("%s"%SeqUtils.GC(precursorSeq))
            splitA = split([[[precursorSeq]]], subseqs)
            for sub in subseqs:
                line.append(sub.id)
                line.append("%s"%sub.seq)
                line.append("%s"%SeqUtils.GC(sub.seq))
            if len(subseqs) < 2:
                line.append('')
                line.append('')
                line.append('')
            for s in splitA:
                line.append(s)
                line.append(str(SeqUtils.GC(s)))
            csvF.write(','.join(line))
            csvF.write('\n')

            # Test for commutability
            subseqs.reverse()
            splitB = split([[[precursorSeq]]], subseqs)
            if(splitA != splitB):
                print("Warning: This split was sensitive to the order of"
                        "subseqences supplied")
                print("splitA (%s)" % splitA)
                print("splitB (%s)" % splitB)
    

def searchForSubseqs(precursors, outfile):
    for p in precursors:
        keys = [ '%s-3p'%p, '%s-5p'%p, p]
        for k in keys:
            subseqs = subseqs_.get(k, None)
            if subseqs:
                preCursorSeq = precursors[p]
                subSeqSeq = subseqs
                subSeqDict[p].append(subSeqSeq)
    splitPrecursor(subSeqDict, outfile)


def main(precursor, subseqs, outfile):
    global precursor_, subseqs_, outfile_
    with open(precursor, "r") as preF:
        precursor_ = SeqIO.to_dict(SeqIO.parse(preF, 'fasta'))
    with open(subseqs, "r") as subF:
        f = SeqIO.parse(subF, 'fasta')
        subseqs_ = SeqIO.to_dict(f)
    searchForSubseqs(precursor_, outfile)

if __name__ == '__main__':
    precursor = sys.argv[1]
    subseqs = sys.argv[2]
    outfile = None
    if len(sys.argv) > 3:
        outfile = sys.argv[3]
    main(precursor, subseqs, outfile)
    plotGCBar(filename = '%s_bar.png' % precursor)
    plotGCLine(filename = '%s_line.png' % precursor)
    plotGCBox(filename = '%s_box.png' % precursor)
