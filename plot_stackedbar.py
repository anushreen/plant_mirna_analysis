#!/usr/bin/env python
    
__author__           = "Anushree Narjala"
__copyright__        = "Copyright 2019, Anushree Narjala and NCBS Bangalore"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Anushree Narjala"
__email__            = "anushreen@ncbs.res.in"
__status__           = "Development"


'''
Usage: python plot_stackedbar.py columnWise_GC_21nt.csv

'''
import numpy as np
import sys
import os
import pandas
import matplotlib.pyplot as plt

def barplot_gc(csvfile):
    data = []
    data = pandas.read_csv(csvfile, delimiter=',').transpose()
    nNuc = data[1].sum()
    normalizedData = 100 * data / data.sum() 
    size = data.transpose().shape[0]
    colors = [  'lightcoral', 'firebrick', 'lightskyblue', 'dodgerblue' ]
    ax = normalizedData.transpose().plot( 
            kind = 'bar', stacked = True
            , color = colors
            , edgecolor = 'w'
            , linewidth=0.5
            , alpha = 0.9
            , fontsize = 12
            )

    ax.set_ylim( [0, 100 ] )
    legend_properties = {'weight':'semibold', 'size':'11'}
    ax.tick_params(top=False, right=False)
    ax.set_xlabel("Position [nt] (%s seqs)" %nNuc, fontsize=14, fontweight = 'bold')
    ax.set_ylabel("GC Percentage", fontsize=14, fontweight = 'bold')
    xlables = range (1, size+1)
    ylables = range (0, 101, 20)
    ax.set_xticklabels(xlables, rotation=0, fontsize=13, fontweight = 'bold')
    ax.set_yticklabels(ylables, rotation=0, fontsize=13, fontweight = 'bold')
    ax.get_xaxis().set_tick_params(direction='out', width=1)
    ax.get_yaxis().set_tick_params(direction='out', width=1)
    ax.legend( bbox_to_anchor=(1.0, 1)
            , loc=2
            , frameon=False
            , prop=legend_properties
            )
    [i.set_linewidth(2) for i in ax.spines.itervalues()]
    plt.tight_layout() #rect = (0, 0, 0.9, 1 ) )

    outfile = '%s_GC.png' % csvfile
    print("Saving figures to %s" % outfile)
    plt.savefig(outfile, dpi = 500) #, transparent=True)
    
def main( csvfile ):
    print('[INFO] Plotting %s' % csvfile)
    barplot_gc( csvfile )

if __name__ == '__main__':
    csvfile = sys.argv[1]
    main( csvfile )
